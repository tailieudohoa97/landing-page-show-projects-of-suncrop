//Script setup on screen visible function
$.fn.visible=function(t,e,o){var wd=$(window); if(!(this.length<1)){var r=this.length>1?this.eq(0):this,n=r.get(0),f=wd.width(),h=wd.height(),o=o?o:"both",l=e===!0?n.offsetWidth*n.offsetHeight:!0;if("function"==typeof n.getBoundingClientRect){var g=n.getBoundingClientRect(),u=g.top>=0&&g.top<h,s=g.bottom>0&&g.bottom<=h,c=g.left>=0&&g.left<f,a=g.right>0&&g.right<=f,v=t?u||s:u&&s,b=t?c||a:c&&a;if("both"===o)return l&&v&&b;if("vertical"===o)return l&&v;if("horizontal"===o)return l&&b}else{var d=wd.scrollTop(),p=d+h,w=wd.scrollLeft(),m=w+f,y=r.offset(),z=y.top,B=z+r.height(),C=y.left,R=C+r.width(),j=t===!0?B:z,q=t===!0?z:B,H=t===!0?R:C,L=t===!0?C:R;if("both"===o)return!!l&&p>=q&&j>=d&&m>=L&&H>=w;if("vertical"===o)return!!l&&p>=q&&j>=d;if("horizontal"===o)return!!l&&m>=L&&H>=w}}}

// Lấy thumb tự động từ ".mySwiper_slide" - Get thumnail auto
$('.mySwiper_thumnail .swiper-wrapper, #intro_projects .album').html('');
$('.mySwiper_slide .swiper-slide > img').each(function(i, el){
    $('.mySwiper_thumnail .swiper-wrapper').append('<div class="swiper-slide"><img src="'+el.src+'" /></div>');
    $('#intro_projects .album')
    .append('<img class="wow fadeInUp" data-wow-delay="'+(Math.random() + 1).toFixed(2)+'s" data-wow-duration="1s" data-wow-offset="-5000" src="'+el.src+'" alt="">');
})

//Start swiper
swiperStart();

//Setting wow nâng cao. Có cũng đc ko có cũng không sao.
//Muốn tối giản hơn thì dùng var wow = new WOW().init() là đc
var wow = new WOW(
{
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         false,      // act on asynchronously loaded content (default is true)
        callback: function(box) {
            // console.log("Running wow", box);
        }
    }
    );

//Start Effect WOW
wow.init();



function swiperStart(){
    var wheelDelay = true;
    var swiper_thumnail = new Swiper(".mySwiper_thumnail", {
        spaceBetween: 0,
        slidesPerView: 6,
        freeMode: true,
        watchSlidesProgress: true,
    });
    var swiper2 = new Swiper(".mySwiper_slide", {
        spaceBetween: 0,
        speed: 1500,
        mousewheel: true,         //Cuộn chuột, Slide sẽ trượt theo
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiper_thumnail,
        },
    });

    // SLIDER SWIPPER
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,   //1 item trên 1 slide (nên mặc định cho màn hình nhỏ nhất)
        spaceBetween: 0,    //Khoảng cách giữa các item (nên mặc định cho màn hình nhỏ nhất)
        slidesPerGroup: 1,  //Trượt 1 cái 1 lần
        // loop: true,         //Vòng lặp
        autoplay:  
        {
            delay: 3000,    //Thời gian trượt
            disableOnInteraction: true, //stop khi nắm kéo 1 item, Cho phép dừng khi tương tác giao diện
        },
        speed: 1500,        //Tốc độ lướt Slide
        loopFillGroupWithBlank: true,
        direction: "vertical",    //hướng dọc
        mousewheel: true,         //Cuộn chuột, Slide sẽ trượt theo
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    swiper.autoplay.stop(); //Lệnh dừng auto play

    //HOVER SẼ DỪNG CHẠY SLIDER
    $('.mySwiper').on('mouseover', function() {
        // swiper.autoplay.stop();
    });

    $('.mySwiper').on('mouseout', function() {
        // swiper.autoplay.start();
    });

    // Nếu muốn check/update cái nào đó mà phải sau khi đổi slide thì viết trong transtionEnd, còn trước khi đổi slide thì trong transitionStart
    // Script sẽ trigger mỗi lần swiper bắt đầu slide
    swiper.on('transitionStart', function() {
        wheelDelay = true;
        wow = new WOW().init();
        swiper2.mousewheel.disable();
        swiper.mousewheel.enable();
    });
    swiper.on('transitionEnd', function() {
        if(swiper.activeIndex == 3){
            // $("#logo").css("background-color","#ffffffd1");
            $("#logo > *").css("filter","brightness( 0% )");
        }
        else{
            $("#logo > *").css("filter","none");
            $("#logo > img").css("filter","contrast(0%) brightness(12.5)");
            // $("#logo").css("background-color","transparent");            
        }
        if($('.mySwiper_slide').visible()){
            swiper.mousewheel.disable();
            wheelDelay = false;
            swiper2.mousewheel.enable();
            return;
        }
        swiper.mousewheel.enable();
    })
    
    //Script sẽ trigger mỗi lần swiper bắt đầu slide
    swiper2.on('transitionStart', function() {
        wow = new WOW().init();
    });
    swiper2.on('transitionEnd', function() {
        if(swiper2.isEnd || swiper2.isBeginning)
            wheelDelay = false;
        // wow = new WOW().init();
    });
    
    //Chỉ bật "cuộn chuột lướt slider" khi đang ở 1 trong 2 slider chính hoặc slider phụ
    $('.home-slider').on("wheel", function(event) {
        if(!wheelDelay){
            wheelDelay = true;
            if (event.originalEvent.wheelDelta >= 0) {
                if(swiper2.isBeginning)
                    swiper.mousewheel.enable();
                console.log('Lướt về slide chính 2'); 
            }
            else {
                if(swiper2.isEnd)
                    swiper.mousewheel.enable();
                console.log('Lướt về slide chính 3'); 
            }
        }
        console.log('Đang trong Slide phụ'); 
    });


    //Click button đến Slider show dự án
    $("#intro .content button").click(function() {
        swiper.slideTo(1); //Từ từ vị trí 0 là slide đầu tiên
    });


    //Click Logo để về slide đầu tiên
    $("#logo img").click(function() {
        swiper.slideTo(0); //Từ từ vị trí 0 là slide đầu tiên
    });


    // Khi click hình ở slider Projects sẽ chuyển đến hình ở Slider Introduce Projects tương ứng
    $('#intro_projects .album img').click(function(e) {
        const imgIndex = $(this).index();   //Lấy vị trí của img đang click
        swiper.slideTo(2);                  //Chuyển đến slide thứ 3 - vị trí 2
        swiper2.slideTo(imgIndex);          //slideTo(Lấy vị trí của img đang click)
    });
}

//Gắn tên vào các "điểm tròn nhấn chuyển slide" như 1 menu - 2 ngôn ngữ
const   swiperSideBar_vi = ['Giới thiệu', 'Các dự án', 'Slide dự án', 'Liên hệ'],
        swiperSideBar_en = ['intro', 'Projects', 'Introduce Projects', 'Contact us'];
$('.mySwiper > .swiper-pagination .swiper-pagination-bullet')
.html((i)=>'\
    <span class="stt"></span>\
    <span class="vi">'+swiperSideBar_vi[i]+'</span>\
    <span class="en" style="display: none;">'+swiperSideBar_en[i]+'</span>\
    ');

$(window).ready(function() {    
    //Đổi ngôn ngữ
    $('.btn-vi').click(function() {
        $('.en, span.swiper-pagination-bullet > span.en, #show_projects .content.position-absolute.overlay-bg-2.en, #contact .cont.en').css('display','none');
        wow = new WOW().init();
        $('.vi').css('display','block');
        $('span.swiper-pagination-bullet > span.vi, #show_projects .content.position-absolute.overlay-bg-2.vi, #contact .cont.vi').css('display','flex');
        console.log('Đã đổi TV'); 
    });
    $('.btn-en').click(function() {
        $('.vi, span.swiper-pagination-bullet > span.vi, #show_projects .content.position-absolute.overlay-bg-2.vi, #contact .cont.vi').css('display','none');
        wow = new WOW().init();
        $('.en').css('display','block');
        $('span.swiper-pagination-bullet > span.en, #show_projects .content.position-absolute.overlay-bg-2.en, #contact .cont.en').css('display','flex');
        console.log('Đã đổi Tiếng Anh'); 
    });
});